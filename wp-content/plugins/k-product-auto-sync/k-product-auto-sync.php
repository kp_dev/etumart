<?php
/*
Plugin Name: K Product Auto Sync
Plugin URI: #
Description: ...
Version: 1.0.0
Author: Cây
Author URI: #
License: A "Slug" license name e.g. GPL2
Text Domain: kpa
*/
if (!defined('ABSPATH')) {
    exit;
}
define('KPA_TEXT_DOMAIN', 'kpa');
define('KPA_VERSION', '1.0.0');
define('KPA_NAME', 'K Product Auto Sync');

if (!class_exists('KPA')) {
    class KPA
    {
        public static $instance;

        public $plugin_url;

        public $plugin_dir;

        public $plugin_base_name;

        public $settings;

        public $post;

        public $file;

        public static function instance()
        {
            if (is_null(self::$instance)) {
                self::$instance = new KPA();
                self::$instance->setup_globals();
                self::$instance->includes();
                self::$instance->setup_actions();
            }
            return self::$instance;
        }

        private function setup_globals()
        {
            $this->file = __FILE__;
            $this->plugin_dir = plugin_dir_path(__FILE__);
            $this->plugin_url = plugin_dir_url(__FILE__);
            $this->plugin_base_name = plugin_basename(__FILE__);
            add_filter('plugin_row_meta', array($this, 'plugin_row_meta'), 10, 2);
            add_action("admin_menu", array($this, "kpa_register_menu"), 10, 1);
            add_action('init', array($this, 'kpa_save_settings'));
            add_action('init', array($this, 'kpa_reg_taxonomy'), 10);
            add_action('admin_enqueue_scripts', array($this, 'kpa_enqueue_scripts'));
        }

        function kpa_register_menu()
        {
            add_menu_page("K Products Sync", "K Products Sync", 'manage_options', 'k-products-auto-sync', array($this, 'kpa_create_dashboard'), 'dashicons-buddicons-activity', 3);
            add_submenu_page("k-products-auto-sync", "Settings", "Settings", 'manage_options', 'k-products-auto-sync', array($this, "kpa_create_dashboard"));
        }

        function kpa_create_dashboard()
        {
            include_once $this->plugin_dir . "/templates/settings.php";
        }

        private function includes()
        {
            require_once $this->plugin_dir . '/inc/kpa-functions.php';
            require_once $this->plugin_dir . '/inc/k-ubuy.php';
        }

        function kpa_enqueue_scripts()
        {
            wp_enqueue_style("kpa.css", kpa()->plugin_url() . "/assets/css/kpa.css", array(), 'all');
            wp_enqueue_script("kpa.js", kpa()->plugin_url() . "/assets/js/kpa.js", array("jquery"), "all", true);

            $kpa_datas = array(
                'url' => admin_url('admin-ajax.php')
            );

            wp_localize_script('kpa.js', 'kpa_datas', $kpa_datas);
        }

        private function setup_actions()
        {
        }

        public function plugin_path()
        {
            return untrailingslashit(plugin_dir_path(__FILE__));
        }

        public function plugin_url()
        {
            return untrailingslashit(plugin_dir_url(__FILE__));
        }

        function plugin_row_meta($plugin_meta, $plugin_file)
        {
            if ($plugin_file !== 'k-product-auto-sync/k-product-auto-sync.php') {
                return $plugin_meta;
            }
            $plugin_meta[] = '<a href="skype:live:kenzy194?chat" title="' . esc_html__('Skype support', KPA_TEXT_DOMAIN) . '">' . esc_html__('Support', KPA_TEXT_DOMAIN) . '</a>';
            $plugin_meta[] = '<a href="mailto:kenzy194@gmail.com" title="' . esc_html__('Send a email to Dev team.', KPA_TEXT_DOMAIN) . '">' . esc_html__('Contact', KPA_TEXT_DOMAIN) . '</a>';

            return $plugin_meta;
        }

        function kpa_save_settings()
        {
            if (!empty($_POST['action']) && $_POST['action'] === 'kpa_save_settings') {
                update_option('kpa_settings', $_POST['kpa_settings']);
            }
        }

        function kpa_reg_taxonomy()
        {
            register_taxonomy(
                'product_brand',
                array('product'),
                array(
                    'hierarchical' => true,
                    'label' => __('Brand', KPA_TEXT_DOMAIN),
                    'labels' => array(
                        'name' => __('Product brands', KPA_TEXT_DOMAIN),
                        'singular_name' => __('Brand', KPA_TEXT_DOMAIN),
                        'menu_name' => _x('Brands', 'Admin menu name', KPA_TEXT_DOMAIN),
                        'search_items' => __('Search brands', KPA_TEXT_DOMAIN),
                        'all_items' => __('All brands', KPA_TEXT_DOMAIN),
                        'parent_item' => __('Parent brand', KPA_TEXT_DOMAIN),
                        'parent_item_colon' => __('Parent brand:', KPA_TEXT_DOMAIN),
                        'edit_item' => __('Edit brand', KPA_TEXT_DOMAIN),
                        'update_item' => __('Update brand', KPA_TEXT_DOMAIN),
                        'add_new_item' => __('Add new brand', KPA_TEXT_DOMAIN),
                        'new_item_name' => __('New brand name', KPA_TEXT_DOMAIN),
                        'not_found' => __('No brands found', KPA_TEXT_DOMAIN),
                    ),
                    'show_ui' => true,
                    'query_var' => true,
                    'capabilities' => array(
                        'manage_terms' => 'manage_product_terms',
                        'edit_terms' => 'edit_product_terms',
                        'delete_terms' => 'delete_product_terms',
                        'assign_terms' => 'assign_product_terms',
                    ),
                    'rewrite' => array(
                        'slug' => "product_brand",
                        'with_front' => false,
                        'hierarchical' => true,
                    ),
                )
            );
        }
    }

    function kpa()
    {
        return KPA::instance();
    }

    $GLOBALS['kpa'] = KPA();
}