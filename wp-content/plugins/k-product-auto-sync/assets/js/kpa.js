(function ($) {
    var kpa_core = {
        init: function () {
            this.handles.kpaUBuySync();
        },
        handles: {
            kpaUBuySync: function () {
                var _ok = true;
                $(document).on("click", ".kpa-ubuy-sync-sm", function (e) {
                    e.preventDefault();
                    var _this = $(this),
                        _urls = $("#kpa-sync-urls").val();
                    if(_urls === ""){
                        _ok = true;
                        return;
                    }
                    if(_ok){
                        _ok = false;
                        $.ajax({
                            url: kpa_datas.url,
                            type: 'POST',
                            beforeSend: function () {
                            },
                            data: {
                                action: 'kpa_ubuy_sync',
                                urls: _urls
                            }
                        })
                            .done(function (data) {
                                if (data.stt === 'done') {

                                }
                            })
                            .fail(function () {
                                return false;
                            })
                            .always(function () {
                                _ok = true;
                                return false;
                            });
                    }
                });
            }
        }
    };
    kpa_core.init();
})(jQuery);
