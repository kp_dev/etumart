<?php
function kpa_get_option( $key, $default ) {
    $settings = get_option( 'kpa_settings', array() );
    if ( isset( $settings[ $key ] ) ) {
        return $settings[ $key ];
    }
    return $default;
}
function kpa_check_asin_exists($asin){
    $args = array(
        'post_type'  => 'product',
        'post_status'  => 'publish',
        'meta_query' => array(
            array(
                'key'   => '_sku',
                'value' => $asin,
            )
        )
    );
    $posts_check =  get_posts( $args );
    if(!empty($posts_check)){
        return true;
    }
    return false;
}