<?php
if (!defined("ABSPATH")) {
    die;
}
if (!class_exists("KPA_Ubuy")) {
    class KPA_Ubuy
    {
        public function __construct()
        {
            add_action("admin_menu", array($this, "kpa_register_ubuy_menu"), 10, 1);
            add_action("wp_ajax_kpa_ubuy_sync", array($this, "kpa_ubuy_sync"));
        }

        function kpa_register_ubuy_menu()
        {
            add_submenu_page("k-products-auto-sync", "Ubuy Sync", "Ubuy Sync", 'manage_options', 'k-ubuy-sync', array($this, "kpa_create_ubuy_sync_page"));
        }

        function kpa_create_ubuy_sync_page()
        {
            include_once kpa()->plugin_dir . "/templates/ubuy.php";
        }

        function kpa_ubuy_sync()
        {
            if (!empty($_POST["urls"])) {
                $links = explode("\n", $_POST["urls"]);
                foreach ($links as $url) {
                    if (strpos($url, "/product/") !== false) {
                        try {
                            $this->kpa_ubuy_product_sync_action($url);
                        }catch(Exception $e){
                            echo "<pre>";
                            var_dump($e);
                            echo "</pre>";
                        }
                    }
                }

            }
        }

        function kpa_ubuy_product_sync_action($link)
        {
            $asin = "";
            $pattern = "/\/product\/.*\/s\//";
            $result = preg_match_all($pattern, $link, $matches);
            if ($result) {
                $asin = str_replace(array("/product/", "/s/"), array("", ""), $matches[0][0]);
            }
            if (empty($asin)) {
                return false;
            }
            if (kpa_check_asin_exists($asin)) {
                return false;
            }
            $url = 'https://www.a.ubuy.com.kw/webpool/common/?dl_store=usstore&dl_action=fetch';
            $ch = curl_init($url);
            $param = array(
                'selected_asin' => $asin,
                'parent_asin' => $asin,
                'is_custom_id_product' => '',
                'storename' => 'store',
                'is_ajax' => 'false'
            );
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_POST, count($param));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
            curl_setopt($ch, CURLOPT_TIMEOUT, 100);
            $output = curl_exec($ch);
            if (strpos($output, "product-not-found") !== false) {
                return false;
            }
            $doc = new DOMDocument();
            $doc->loadHTML($output);

            $datas = array();
            foreach ($doc->getElementsByTagName('script') as $key => $val) {
                $t_v = $val->textContent;
                if (strpos($t_v, "setCurrencyCode") !== false) {
                    continue;
                }
                if (strpos($t_v, "pObj.category") !== false) {
                    $pattern = "/postdata: .*},/";
                    $result = preg_match_all($pattern, $t_v, $matches);
                    if ($result) {
                        $d_t = str_replace("postdata: ", "", $matches[0][0]);
                        $n_d = substr($d_t, 0, strrpos($d_t, "},"));
                        $datas['product_info'] = json_decode($n_d, true);
                    }
                    $pattern = "/pObj.price.*';/";
                    $result = preg_match_all($pattern, $t_v, $matches);
                    if ($result) {
                        $datas["price"] = round(str_replace(array("pObj.price = '", "';"), array("", ""), $matches[0][0]) * kpa_get_option("kwd_usd", 0), 2);
                    } else {
                        $datas["price"] = 0;
                    }
                }
                if (strpos($t_v, "variations: '") !== false) {
                    $pattern = "/variations: '.*]',/";
                    $result = preg_match_all($pattern, $t_v, $matches);
                    if ($result) {
                        $d_t_vari = str_replace(array("variations: '", "',", "\\\""), array("", "", "\""), $matches[0][0]);
                        $datas["variations"] = json_decode($d_t_vari, true);
                    }
                }
            }
            $p_cates = explode(",", implode(",", $datas["product_info"]["fArr"]));
            $product_categories_s = array();
            //Product categories handles
            foreach ($p_cates as $d => $cates) {
                $t_e = term_exists($cates, "product_cat");
                if (empty($t_e)) {
                    $p_t = array();
                    if ($d != 0) {
                        $t_e_p = term_exists($p_cates[$d - 1], "product_cat");
                        $p_t = array(
                            "parent" => intval($t_e_p["term_id"])
                        );
                    }
                    $rss = wp_insert_term($cates, "product_cat", $p_t);
                    $product_categories_s[] = intval($rss["term_id"]);
                }else{
                    $product_categories_s[] = intval($t_e["term_id"]);
                }
            }
            //Product brand handles
            $brand_exists = term_exists($datas["product_info"]["product_brand"], "product_brand");
            if (empty($brand_exists)) {
                $cr_p_brand = wp_insert_term($datas["product_info"]["product_brand"], "product_brand");
                $p_brand = $cr_p_brand["term_id"];
            } else {
                $p_brand = $brand_exists["term_id"];
            }

            //Images handles
            $imgs_source = $doc->getElementById("product-main-images");
            $ul_nodes = null;
            foreach ($imgs_source->childNodes as $x) {
                if ($x->tagName == "ul") {
                    $ul_nodes = $x;
                    break;
                }
            }
            $gallery_list = array();
            $pr_thumbnail = "";
            $ind = 0;
            foreach ($ul_nodes->getElementsByTagName("img") as $img_tag) {
                if ($ind != 0) {
                    $gallery_list[] = $this->kpa_ubuy_insert_attachment_from_url($img_tag->getAttribute('data-src'));
                } else {
                    $pr_thumbnail = $this->kpa_ubuy_insert_attachment_from_url($img_tag->getAttribute('data-src'));
                }
                $ind++;
            }

            //Additional handles
            $addition_data = array();
            foreach (json_decode($datas["product_info"]["additional_information"], true) as $key => $addition) {
                $addition_data[] = array(
                    'name' => $addition["title"],
                    'value' => $addition["value"],
                    'position' => $key,
                    'is_visible' => "1",
                    'is_variation' => "0",
                    'is_taxonomy' => "0"
                );
            }

            $short_des = str_replace("KP||KP", "</li><li>", str_replace(array("\"]", "[\""), array("</li>", "<li>"), str_replace("\",\"", "KP||KP", $datas["product_info"]["short_description"])));
            $post_arr = array(
                'post_title' => $datas["product_info"]["name"],
                'post_content' => $datas["product_info"]["description"],
                'post_status' => 'publish',
                'post_type' => 'product',
                'post_excerpt' => $short_des,
            );
            $pid = wp_insert_post($post_arr);
            if (!is_wp_error($pid)) {
                wp_set_object_terms($pid, $product_categories_s, "product_cat",true);
                wp_set_object_terms($pid, intval($p_brand), "product_brand",true);
                update_post_meta($pid, "_sku", $asin);
                update_post_meta($pid, "_thumbnail_id", $pr_thumbnail);
                update_post_meta($pid, "_product_image_gallery", implode(",", $gallery_list));
                update_post_meta($pid, "_regular_price", $datas["price"]);
                update_post_meta($pid, "_sale_price", round(($datas["price"] * (100 - intval(kpa_get_option("sale_percent", 0)))) / 100)-0.01);
                update_post_meta($pid, "_price", round(($datas["price"] * (100 - intval(kpa_get_option("sale_percent", 0)))) / 100)-0.01);
                update_post_meta($pid, "_product_attributes", $addition_data);

                //Variation handles
                $vari_dt = array();
                $asin_list = array();
                if (!empty($datas["variations"][0]["heading"]) && $datas["variations"][0]["heading"] == "Color") {
                    foreach ($datas["variations"][0]["options"] as $n_asin => $vari_name) {
                        $vari_dt[] = $n_asin . "|" . $vari_name["value"];
                        $asin_list[] = $n_asin;
                    }
                    update_post_meta($pid, "_product_color", implode(",", $vari_dt));
                    foreach ($asin_list as $n_asin_to_sync) {
                        $this->kpa_ubuy_product_sync_action("/view/product/" . $n_asin_to_sync . "/s/");
                    }
                }
            }
            die("Done");
        }

        function kpa_ubuy_insert_attachment_from_url($url, $parent_post_id = null)
        {
            if (!class_exists('WP_Http'))
                include_once(ABSPATH . WPINC . '/class-http.php');
            $http = new WP_Http();
            $response = $http->request($url);
            if ($response['response']['code'] != 200) {
                return false;
            }
            $upload = wp_upload_bits(basename($url), null, $response['body']);
            if (!empty($upload['error'])) {
                return false;
            }
            $file_path = $upload['file'];
            $file_name = basename($file_path);
            $file_type = wp_check_filetype($file_name, null);
            $attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
            $wp_upload_dir = wp_upload_dir();

            $post_info = array(
                'guid' => $wp_upload_dir['url'] . '/' . $file_name,
                'post_mime_type' => $file_type['type'],
                'post_title' => $attachment_title,
                'post_content' => '',
                'post_status' => 'inherit',
            );
            // Create the attachment
            $attach_id = wp_insert_attachment($post_info, $file_path, $parent_post_id);

            // Include image.php
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            // Define attachment metadata
            $attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

            // Assign metadata to attachment
            wp_update_attachment_metadata($attach_id, $attach_data);

            return $attach_id;

        }
    }

    new KPA_Ubuy();
}
