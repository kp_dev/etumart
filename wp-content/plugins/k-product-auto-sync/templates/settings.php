<h1>KPA Settings</h1>
<form method="post" class="kpa-settings-page-wrap">
    <div class="kpa-st-gr">
        <label>% of Sale</label>
        <input type="text" name="kpa_settings[sale_percent]" value="<?php echo kpa_get_option("sale_percent","") ?>">
    </div>
    <div class="kpa-st-gr">
        <label>1 KWD = ? USD</label>
        <input type="text" name="kpa_settings[kwd_usd]" value="<?php echo kpa_get_option("kwd_usd","") ?>">
    </div>
    <input type="hidden" name="kpa_settings[settings]" value="true">
    <input type="hidden" name="action" value="kpa_save_settings">
    <div class="kpa-st-gr kpa-action">
        <button class="cshab-save-settings button button-primary"><?php esc_html_e('Save Settings', KPA_TEXT_DOMAIN) ?></button>
    </div>
</form>