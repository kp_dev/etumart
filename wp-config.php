<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'etu' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mysql' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'j~Y`kIANXuKQ(YHWlOQcL%,v}e~yWufx*5g:Q.Q/Xkcr 9 v}W^ke=YVq?;N+Ymu' );
define( 'SECURE_AUTH_KEY',  '}PT[ml` D+@iaadr3r&h#PKT=G3z|u)Vx&Qx-$mA{h*RQ![}|Bi+49u]&Hsse=%7' );
define( 'LOGGED_IN_KEY',    ';V^ZD1;X6`x/[|gY&Si?L`a!0w,zW Om&`$9j-bQKuw1Cym-`Ga(.9{H`LfsUj1+' );
define( 'NONCE_KEY',        'CL<Hq7SU09%DuzFEPed-T]K3ljY+Q6la`(+z:^~RrQ1Q6kvz}=-3h4[HOM&*Y/j|' );
define( 'AUTH_SALT',        'N;D.rNFpYmcEAXBWfPjyQ8E}3*l4//mFB}DR40?6A>B~L*[7X#sP[OWA*-qxyn5{' );
define( 'SECURE_AUTH_SALT', '!M0D5s7z<hLgfmVgCFMw*TX#rLP 7pR[N{)icVM$=NuY;1b7D5!Dc`(W;x]E5fD+' );
define( 'LOGGED_IN_SALT',   'RgQZu=7m$8Gs#@p(s90_KW@4eFRV`}#uC`r|:!=:Vd+kY3=JjfBIj9IBWrRNupCr' );
define( 'NONCE_SALT',       'J1L0T|FEaS[&5GzMY=1(.f1+`9uKdQgCg?nS=<)[Mw JaDx<EEpZ&:1@Yy{_LL&4' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
